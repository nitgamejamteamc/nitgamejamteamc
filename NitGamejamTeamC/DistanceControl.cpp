#include "DistanceControl.h"

DistanceControl::DistanceControl(int posX, int posY) :
	Position(posX, posY)
{
}

void DistanceControl::SetDistance(int playerPosX, int playerPosY)
{
	int diff = playerPosX - Position.first;
	AxisDistance.first = playerPosX > Position.first ? diff : -diff;

	diff = playerPosY - Position.second;
	AxisDistance.second = playerPosY > Position.second ? diff : -diff;
}