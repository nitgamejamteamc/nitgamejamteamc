#pragma once
#include <memory>

class DistanceControl
{
public:
	DistanceControl(int posX, int posY);
	~DistanceControl() = default;

	void SetDistance(int playerPosX, int playerPosY);

	bool IsDistanceZero()
	{
		return AxisDistance.first == 0 && AxisDistance.second == 0;
	}

	int GetDistanceX()
	{
		return AxisDistance.first;
	}

	int GetDistanceY()
	{
		return AxisDistance.second;
	}

private:
	std::pair<int, int> AxisDistance;
	std::pair<int, int> Position;
};