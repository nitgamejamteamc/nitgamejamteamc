#pragma once
# include <Siv3D.hpp>

class Player;

class FloorBase
{
public:
	virtual bool gimmick(Player* player, bool& isEvent, String& assetName);/*それぞれのギミックの動作を定義する*/

	FloorBase();

	bool GetFinish()
	{
		return finish;
	}

	virtual void textureDraw() const;

protected:
	int drawCount; /*アニメーションがつくなら使う*/

	bool finish;
private:
};