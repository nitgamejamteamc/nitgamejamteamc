#include "GameScene.h"

GameMain::GameMain() :
	m_Player(19, 19)
{

}

void GameMain::init()
{
	m_Player.SetMamName(m_data->mamaName);
}

void GameMain::update()
{
	if (m_Player.Update())
	{
		m_data->isGet = m_Player.GetIsGet();
		changeScene(L"Result");
	}
}

void GameMain::draw() const
{
	m_Player.Draw();
}