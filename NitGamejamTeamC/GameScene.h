#pragma once
#include "MyAppDefine.h"
#include "Player.h"

struct GameMain : MyApp
{
public:

	GameMain();
	~GameMain() = default;

	void init() override;
	void update() override;
	void draw() const override;

	Player m_Player;
};