# include "TreasureBoxGimmick.h"
# include "PictureFlameGimmick.h"
# include "GuideGimmick.h"
# include "Player.h"
/*Floor*/
FloorBase::FloorBase():drawCount(0),finish(false)
{
}

bool FloorBase::gimmick(Player* player, bool& isEvent, String& assetName)
{
	isEvent = false;
	return false;
}

void FloorBase::textureDraw() const
{

}

/*TreasureBox*/
TreasureBox::TreasureBox():
	money(Random(0,1200)),
	examined(false),
	FloorBase()
{
}

bool TreasureBox::gimmick(Player* Player,bool& isEvent,String& assetName)
{
	if (isEvent)
	{
		if (examined)
			return false;
		Player->SetFunds(money);
		assetName = L"treasureBoxOpened";
		examined = true;
		isEvent = false;
	}
	else
		assetName = L"treasureBox";

	return false;
}

void TreasureBox::textureDraw() const
{

}

/*PictureFlame*/
PictureFlame::PictureFlame(MamaName& name) :
	cost(-3000),
	status(false),
	FloorBase(),
	mamaname(name),
	timer(0)
{
}

bool PictureFlame::gimmick(Player* player, bool& isEvent, String& assetName)
{
	if (isEvent)
	{
		if (timer > 0)
		{
			if (--timer == 0)
			{
				isEvent = false;
				return true;
			}
		}
		else if (timer < 0)
		{
			if (++timer == 0)
				isEvent = false;
		}
		else if (player->GetFund() >= 2000)
		{
			player->LoseMoney(1500);
			status = true;
			if (mamaname == MamaName::miyu)
				assetName = L"miyuCurtain";
			else if (mamaname == MamaName::miria)
				assetName = L"miriaCurtain";
			else if (mamaname == MamaName::momoka)
				assetName = L"momokaCurtain";

			timer = 90;
		}
		else
		{
			assetName = L"curtain";
			timer = -30;
		}
	}

	return false;
}

void PictureFlame::textureDraw() const
{
	if (timer < 0)
		font(L"�����˂��I").drawAt(512, 300);
}

/*Guide*/
Guide::Guide() :cost(3000), FloorBase(), m_isHint(false)
{
}

bool Guide::gimmick(Player* Player, bool& isEvent, String& assetName)
{
	if (isEvent)
	{
		if (!m_isHint)
		{
			Player->LoseMoney();
			m_isHint = true;
		}
		else
		{
			if (Input::KeyF.clicked)
			{
				m_isHint = false;
				isEvent = false;
			}
		}
	}

	return false;
}

void Guide::textureDraw() const
{
	TextureAsset(L"Hinto").scale(0.5).drawAt(512, 384);
	font(L"press F key").drawAt(512, 600);
}