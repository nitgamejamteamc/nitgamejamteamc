#pragma once
# include "FloorClass.h"

class Guide:public FloorBase
{
public:
	Guide();
	virtual bool gimmick(Player* player, bool& isEvent, String& assetName);
	virtual void textureDraw() const;
private:
	const int cost;
	bool m_isHint;
	Font font;
};