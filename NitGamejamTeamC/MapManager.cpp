#include "MapManager.h"
#include "MyAppDefine.h"

MapManager::MapManager(String fileName) :
	TextureAssetName(L"Up0"),
	checkedWallFlags(14),
	m_eventIndex(0),
	m_isEvent(false),
	changeUpTexture(0),
	appearedExplanation(controlExplanation::None)
{
	CSVReader csv(fileName);
	for (std::size_t i = 0; i < csv.columns(0); ++i)
	{
		m_MapInfo.emplace_back(std::vector<MapChip>());
		for (size_t j = 0; j < csv.rows; ++j)
		{
			int getValue = csv.get<int>(j, i);
			m_MapInfo[i].emplace_back(static_cast<MapChip>(getValue));
			if (getValue == static_cast<int>(MapChip::PictureFrame))
			{
				DistanceList.emplace_back(std::make_shared<DistanceControl>(i, j));
			}
		}
	}

	for (auto distance : DistanceList)
	{
		distance->SetDistance(19, 19);
	}

	m_MapSize = std::pair<std::size_t, std::size_t>(csv.columns(0), csv.rows);
}

void MapManager::SetMamName(MamaName mamName)
{
	m_EventList.emplace_back(std::make_shared<FloorBase>());
	m_EventList.emplace_back(std::make_shared<Guide>());
	m_EventList.emplace_back(std::make_shared<PictureFlame>(mamName));
	m_EventList.emplace_back(std::make_shared<TreasureBox>());
}

bool MapManager::Event(Player* player, int playerPosX, int playerPosY)
{
	m_MapInfo[playerPosX][playerPosY] = MapChip::Normal;
	return m_EventList[m_eventIndex]->gimmick(player, m_isEvent, TextureAssetName);
}

void MapManager::Draw() const
{
	TextureAsset(TextureAssetName).draw();
	//font(TextureAssetName).draw();
	//font(checkedWallFlags).draw(0, 300);

	for (int dir = 0; dir < maxDirectionCount; ++dir)
	{
		if ((checkedWallFlags & (1 << dir)) == 0)
		{
			switch (dir)
			{
			case 0: TextureAsset(L"Arrow" + ToString(dir)).draw(Window::Center()+Point(-70,0)); break;
			case 1: TextureAsset(L"Arrow" + ToString(dir)).draw(Window::Center()+Point(10,50)); break;
			case 2: TextureAsset(L"Arrow" + ToString(dir)).draw(Window::Center()+Point(-70,100)); break;
			case 3: TextureAsset(L"Arrow" + ToString(dir)).draw(Window::Center()+Point(-150,50)); break;
			}
		}
	}

	switch (appearedExplanation)
	{
	case controlExplanation::Open:
		TextureAsset(L"Open").drawAt(512, 300);
		break;
	case controlExplanation::Turn:
		TextureAsset(L"Turn").drawAt(512, 300);
		break;
	}

	if (m_isEvent)
	{
		m_EventList[m_eventIndex]->textureDraw();
	}
}

bool MapManager::IsAbleToMove(DirValue moveDir, int& playerPosX, int& playerPosY, int& playerDir)
{
	int correctDir = (playerDir + moveDir) % maxDirectionCount;

	switch (correctDir)
	{
	case ToUp:
		if (playerPosY - 1 > 0 && m_MapInfo[playerPosX][playerPosY - 1] != MapChip::Wall)
		{
			--playerPosY;
			playerDir = correctDir;
			SetTextureAssetName(playerPosX, playerPosY, playerDir);
			return true;
		}
		break;
	case ToRight:
		if (playerPosX + 1 < m_MapSize.first && m_MapInfo[playerPosX + 1][playerPosY] != MapChip::Wall)
		{
			++playerPosX;
			playerDir = correctDir;
			SetTextureAssetName(playerPosX, playerPosY, playerDir);
			return true;
		}
		break;
	case ToDown:
		if (playerPosY + 1 < m_MapSize.second && m_MapInfo[playerPosX][playerPosY + 1] != MapChip::Wall)
		{
			++playerPosY;
			playerDir = correctDir;
			SetTextureAssetName(playerPosX, playerPosY, playerDir);
			return true;
		}
		break;
	case ToLeft:
		if (playerPosX - 1 > 0 && m_MapInfo[playerPosX - 1][playerPosY] != MapChip::Wall)
		{
			--playerPosX;
			playerDir = correctDir;
			SetTextureAssetName(playerPosX, playerPosY, playerDir);
			return true;
		}
		break;
	}

	return false;
}

void MapManager::SetTextureAssetName(int playerPosX, int playerPosY, int& playerDir)
{
	checkedWallFlags = static_cast<int>(RootPattern::All);

	//check left
	if (m_MapInfo[playerPosX - 1][playerPosY] == MapChip::Wall)
	{
		checkedWallFlags |= (1 << + ((ToLeft - playerDir + maxDirectionCount) % 4));
	}

	//check up
	if (m_MapInfo[playerPosX][playerPosY - 1] == MapChip::Wall)
	{
		checkedWallFlags |= (1 << ((ToUp - playerDir + maxDirectionCount) % 4));
	}

	//check right
	if (m_MapInfo[playerPosX + 1][playerPosY] == MapChip::Wall)
	{
		checkedWallFlags |= (1 << ((ToRight - playerDir + maxDirectionCount) % 4));
	}

	//check down
	if (m_MapInfo[playerPosX][playerPosY + 1] == MapChip::Wall)
	{
		checkedWallFlags |= (1 << ((ToDown - playerDir + maxDirectionCount) % 4));
	}

	appearedExplanation = controlExplanation::None;

	if (m_MapInfo[playerPosX][playerPosY] >= MapChip::Guide)
	{
		switch (m_MapInfo[playerPosX][playerPosY])
		{
		case MapChip::Guide:
			TextureAssetName = L"Guide";
			playerDir = ToRight;
			checkedWallFlags = 1;
			m_eventIndex = 1;
			appearedExplanation = controlExplanation::Turn;
			break;
		case MapChip::PictureFrame:
			TextureAssetName = L"curtain";
			appearedExplanation = controlExplanation::Open;
			m_eventIndex = 2;
			break;
		case MapChip::TreasureBox:
			TextureAssetName = L"treasureBox";
			appearedExplanation = controlExplanation::Open;
			m_eventIndex = 3;
			break;
		}

		return;
	}

	switch (static_cast<RootPattern>(checkedWallFlags))
	{
	case RootPattern::All:
		TextureAssetName = L"All";
		break;
	case RootPattern::Up:
		TextureAssetName = L"Up";
		changeUpTexture += 1;
		TextureAssetName += ToString(changeUpTexture);
		break;
	case RootPattern::UpLeft:
		TextureAssetName = L"UpLeft";
		break;
	case RootPattern::UpRight:
		TextureAssetName = L"UpRight";
		break;
	case RootPattern::Left:
		TextureAssetName = L"Left";
		break;
	case RootPattern::Right:
		TextureAssetName = L"Right";
		break;
	case RootPattern::RightLeft:
		TextureAssetName = L"RightLeft";
		break;
	case RootPattern::None:
		TextureAssetName = L"None";
		break;
	}

	return;
}

bool MapManager::IsRight(int playerPosY)
{
	int dir = 0;

	for (int i = 1; i < DistanceList.size(); ++i)
	{
		if (DistanceList[dir]->GetDistanceY() > DistanceList[i]->GetDistanceY())
		{
			dir = i;
		}
	}

	if (playerPosY < DistanceList[dir]->GetDistanceY())
	{
		return true;
	}

	return false;
} 