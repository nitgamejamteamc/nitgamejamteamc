#pragma once
#include <vector>
#include <memory>
#include <Siv3D.hpp>
#include "GuideGimmick.h"
#include "PictureFlameGimmick.h"
#include "TreasureBoxGimmick.h"
#include "DistanceControl.h"

enum class controlExplanation
{
	None,
	Open,
	Turn
};

enum DirValue
{
	ToUp,
	ToRight,
	ToDown,
	ToLeft
};

enum class RootPattern
{
	All = 0,
	RightLeft = 1,
	UpLeft = 2,
	Left = 3,
	UpRight = 8,
	Right = 9,
	Up = 10,
	None = 11

};

enum class MapChip
{
	Normal,
	Wall,
	Guide,
	PictureFrame,
	TreasureBox
};

class MapManager
{
public:
	MapManager(String fileName);
	~MapManager() = default;

	bool Event(Player* player, int playerPosX, int playerPosY);
	void Draw() const;

	bool IsAbleToMove(DirValue moveDir, int& playerPosX, int& playerPosY, int& playerDir);

	void SetMamName(MamaName mamName);

	bool IsRight(int playerPosY);

	bool GetIsEvent()
	{
		return m_isEvent;
	}

	void SetIsEvent(bool flag)
	{
		m_isEvent = flag;
	}

	void SetNone()
	{
		appearedExplanation = controlExplanation::None;
	}

private:
	void SetTextureAssetName(int playerPosX, int playerPosY, int& playerDir);

	std::vector<std::vector<MapChip>> m_MapInfo;
	std::pair<std::size_t, std::size_t> m_MapSize;
	std::vector<std::shared_ptr<FloorBase>> m_EventList;
	std::vector<std::shared_ptr<DistanceControl>> DistanceList;

	String TextureAssetName;

	unsigned int checkedWallFlags;

	static const int maxDirectionCount = 4;

	bool m_isEvent;

	int m_eventIndex;

	unsigned int changeUpTexture : 1;

	Font font;

	controlExplanation appearedExplanation;
};