#pragma once

#include <Siv3D.hpp>
#include <HamFramework.hpp>
#include "enumClassMama.h"

struct CommonData
{
	MamaName mamaName;
	bool isGet;
};

enum AlphaManager
{
	increase,
	decrease
};


using MyApp = SceneManager<String, CommonData>::Scene;