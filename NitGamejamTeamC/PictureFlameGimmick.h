#pragma once
# include "FloorClass.h"
# include "enumClassMama.h"

class PictureFlame:public FloorBase
{
public:
	PictureFlame(MamaName& mamaname); /*m_dataを参照*/
	virtual bool gimmick(Player* player, bool& isEvent, String& assetName);
	virtual void textureDraw() const;
private:
	const int cost;
	MamaName mamaname;
	bool status;/*false:絵が隠れてる、true:絵が見えてる(お金を払った後)*/
	int timer;
	Font font;
};

