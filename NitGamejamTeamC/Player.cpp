#include "Player.h"

Player::Player(int firstPosX, int firstPosY) :
	m_PlayerPos(firstPosX, firstPosY),
	m_currentPlayerDir(ToLeft),
	m_isAlive(true),
	m_funds(3000),
	m_MapManager(L"./map/map.csv"),
	m_isGet(false),
	m_diplayFunds(3000)
{
	
}

bool Player::Update()
{
	if (!m_MapManager.GetIsEvent())
	{
		if (Input::KeyUp.clicked && m_MapManager.IsAbleToMove(ToUp, m_PlayerPos.first, m_PlayerPos.second, m_currentPlayerDir))
		{
			LoseMoney();
		}
		else if (Input::KeyDown.clicked && m_MapManager.IsAbleToMove(ToDown, m_PlayerPos.first, m_PlayerPos.second, m_currentPlayerDir))
		{
			LoseMoney();
		}
		else if (Input::KeyRight.clicked && m_MapManager.IsAbleToMove(ToRight, m_PlayerPos.first, m_PlayerPos.second, m_currentPlayerDir))
		{
			LoseMoney();
		}
		else if (Input::KeyLeft.clicked && m_MapManager.IsAbleToMove(ToLeft, m_PlayerPos.first, m_PlayerPos.second, m_currentPlayerDir))
		{
			LoseMoney();
		}
		else if (Input::KeyF.clicked)
		{
			m_MapManager.Event(this, GetPlayerPosX(), GetPlayerPosY());
			m_MapManager.SetIsEvent(true);
			m_MapManager.SetNone();
		}

		if (m_funds <= 0)
		{
			return true;
		}
	}
	else
	{
		m_isGet = m_MapManager.Event(this, GetPlayerPosX(), GetPlayerPosY());
		return m_isGet;
	}

	if (m_funds > m_diplayFunds)
	{
		++m_diplayFunds;
	}
	else if (m_funds < m_diplayFunds)
	{
		--m_diplayFunds;
	}

	return false;
}

void Player::Draw() const
{
	m_MapManager.Draw();

	TextureAsset(L"Coin").resize(100, 100).draw(700, 0);
	font(m_diplayFunds).draw(850, 30);
}