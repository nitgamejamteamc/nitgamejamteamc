#pragma once
#include <map>
#include <vector>
#include <Siv3D.hpp>
#include "MapManager.h"

class Player
{
public:
	Player(int firstPosX, int firstPosY);

	bool Update();
	void Draw() const;

	Font font;

	void LoseMoney(int value = 0)
	{
		m_funds -= lostMoneyAmount + value;
	}

	void IncreaseMoney()
	{
		m_funds += gotMoneyAmount;
	}

	bool GetIsAlive()
	{
		return m_isAlive;
	}

	void  SetFunds(int value)
	{
		m_funds += value;
	}

	int GetFund()
	{
		return m_funds;
	}

	int GetPlayerPosX()
	{
		return m_PlayerPos.first;
	}

	int GetPlayerPosY()
	{
		return m_PlayerPos.second;
	}

	void SetMamName(MamaName mamName)
	{
		m_MapManager.SetMamName(mamName);
	}

	bool IsPictureRight()
	{
		return m_MapManager.IsRight(m_PlayerPos.second);
	}

	bool GetIsGet()
	{
		return m_isGet;
	}

private:
	std::pair<int, int> m_PlayerPos;

	//std::vector<std::vector<int>> m_MapInfo;
	//std::pair<int, int> m_MapSize;

	int m_currentPlayerDir;
	bool m_isAlive, m_isGet;
	int m_funds, m_diplayFunds;

	static const int gotMoneyAmount = 500;
	static const int lostMoneyAmount = 30;

	MapManager m_MapManager;
};