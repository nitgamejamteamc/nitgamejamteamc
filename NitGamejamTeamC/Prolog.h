#pragma once
# include "MyAppDefine.h"

enum SceneInProlog
{
	explanation,
	mamaselect
};

struct Prolog : MyApp
{
public:
	Prolog();
	~Prolog() = default;

	void init() override;
	void update() override;
	void draw() const override;

private:
	TextReader textreader{L"textTest/prolog.txt"};
	Font font;
	String string;
	int alpha;
	bool alphaManager;
	void alphaFunction();
	int textPosY;
	Color fontColor;
	const Rect collision{810,250,140,110};
	void soundPlay();
	void mamaSelect();
	int sceneInProlog;
	Rect blackCurtain;
	int animationCount;
};