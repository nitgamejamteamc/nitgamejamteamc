# include "Prolog.h"

Prolog::Prolog()
{

}

void Prolog::init()
{
	alpha = 0;
	textPosY = Window::Height();
	alphaManager = increase;
	font = Font(30);
	textreader.readAll(string);
	fontColor = Color(255,255, 255, alpha);
	sceneInProlog = explanation;
	blackCurtain = Rect(0, 0, Window::Width(), Window::Height());
	m_data->mamaName = MamaName::miyu;
	animationCount = 0;
}

void Prolog::update()
{
	textPosY -= 2;
	alphaFunction();
	soundPlay();
	


	if (sceneInProlog == mamaselect)
	{
		++animationCount;
		if (Input::KeyLeft.clicked)
		{
			if (!SoundAsset(L"selectSE").isPlaying())
				SoundAsset(L"selectSE").play();
			else if (SoundAsset(L"selectSE").isPlaying())
			{
				SoundAsset(L"selectSE").stop();
				SoundAsset(L"selectSE").play();
			}
			if (m_data->mamaName == MamaName::miria)
				;
			else if (m_data->mamaName == MamaName::momoka)
				m_data->mamaName = MamaName::miria;
			else if (m_data->mamaName == MamaName::miyu)
				m_data->mamaName = MamaName::momoka;
		}
		else if (Input::KeyRight.clicked)
		{
			if (!SoundAsset(L"selectSE").isPlaying())
				SoundAsset(L"selectSE").play();
			else if (SoundAsset(L"selectSE").isPlaying())
			{
				SoundAsset(L"selectSE").stop();
				SoundAsset(L"selectSE").play();
			}
			if (m_data->mamaName == MamaName::miria)
				m_data->mamaName = MamaName::momoka;
			else if (m_data->mamaName == MamaName::momoka)
				m_data->mamaName = MamaName::miyu;
			else if (m_data->mamaName == MamaName::miyu)
				;
		}
		if (Input::KeySpace.clicked)
		{
			if (!SoundAsset(L"selectSE").isPlaying())
				SoundAsset(L"selectSE").play();
			else if (SoundAsset(L"selectSE").isPlaying())
			{
				SoundAsset(L"selectSE").stop();
				SoundAsset(L"selectSE").play();
			}
			changeScene(L"Game");
		}
	}
	else if (sceneInProlog == explanation)
	{
		if (Input::KeySpace.clicked)
		{
			if (!SoundAsset(L"selectSE").isPlaying())
				SoundAsset(L"selectSE").play();
			else if (SoundAsset(L"selectSE").isPlaying())
			{
				SoundAsset(L"selectSE").stop();
				SoundAsset(L"selectSE").play();
			}
			sceneInProlog = mamaselect;
		}
	}
}

void Prolog::draw() const
{	
	if (sceneInProlog == mamaselect)
	{
		if (m_data->mamaName == MamaName::miyu)
		{
		
			TextureAsset(L"miyuSelect").draw();
			if (animationCount % 60 >= 0 && animationCount % 60 < 30)
			{
				TextureAsset(L"leftArrow").draw(Window::Center() + Point(-300, 0));
			}
		}
		else if (m_data->mamaName == MamaName::miria)
		{
			TextureAsset(L"miriaSelect").draw();
			if (animationCount % 60 >= 0 && animationCount % 60 < 30)
			{
				TextureAsset(L"rightArrow").draw(Window::Center() + Point(180, 0));
			}
		}
		else if (m_data->mamaName == MamaName::momoka)
		{
			TextureAsset(L"momokaSelect").draw();
			if (animationCount % 60 >= 0 && animationCount % 60 < 30)
			{
				TextureAsset(L"leftArrow").draw(Window::Center() + Point(-300, 0));
				TextureAsset(L"rightArrow").draw(Window::Center() + Point(180, 0));
			}
		}
		font(L"Press Space to Decide").drawAt(500,650, Palette::White);
	}
	else if (sceneInProlog == explanation)
	{
		TextureAsset(L"mamaSelect").draw();
		blackCurtain.draw({ 0,0,0,150 });
		if (!SoundAsset(L"ogyaru").isPlaying())
		{
			TextureAsset(L"kimoota").resize(360, 639).draw(Window::Width() - TextureAsset(L"kimoota").width + 180, Window::Height() - TextureAsset(L"kimoota").height + 300);
		}
		else
		{
			TextureAsset(L"kimootaSabun").resize(360, 639).draw(Window::Width() - TextureAsset(L"kimootaSabun").width + 180, Window::Height() - TextureAsset(L"kimootaSabun").height + 300);
		}
		font(string).draw(80, textPosY, Alpha(150));
		font(L"Press Space").draw(400, 600, fontColor);
	}
}

void Prolog::alphaFunction()
{
	if (alphaManager == increase)
		alpha += 3;
	else if (alphaManager == decrease)
		alpha -= 3;
	if ((alpha > 255) && (alphaManager == increase))
	{
		alpha = 255;
		alphaManager = decrease;
	}
	else if ((alpha < 0) && (alphaManager == decrease))
	{
		alpha = 0;
		alphaManager = increase;
	}

	fontColor.a = alpha;
}

void Prolog::soundPlay()
{
	
	if (Input::KeyY.clicked || collision.leftClicked)
	{
		if (!SoundAsset(L"ogyaru").isPlaying())
			SoundAsset(L"ogyaru").play();
		else if (SoundAsset(L"ogyaru").isPlaying())
		{
			SoundAsset(L"ogyaru").stop();
			SoundAsset(L"ogyaru").play();
		}
	}
}

void Prolog::mamaSelect()
{

}