#include "ResultScene.h"

Result::Result()
{

}

void Result::init()
{
	SoundAsset(L"OnTheWay").stop(2.0s);
	SoundAsset(L"gameclear").setLoop(true);
	SoundAsset(L"gameover").setLoop(true);
	textreader.readAll(string);
	textPosY = Window::Height();
	font = Font(30);
	countText = 0;

}

void Result::update()
{
	if (countText <= 300)
		++countText;
	if(m_data->isGet) 
	{
		textPosY -= 2;
		SoundAsset(L"gameclear").play(3.0s);
	}
	else if (!m_data->isGet)
	{
		SoundAsset(L"gameover").play(3.0s);
	}
	if (Input::KeySpace.clicked)
	{
		SoundAsset(L"selectSE").play();
		SoundAsset(L"gameover").stop(2.0s);
		SoundAsset(L"gameclear").stop(2.0s);

		changeScene(L"Title");
	}
}

void Result::draw() const	
{
	if (m_data->isGet)
	{
		SoundAsset(L"gameclear").play(3.0s);
		if (m_data->mamaName == MamaName::miyu)
			TextureAsset(L"miyuResult").draw();
		else if (m_data->mamaName == MamaName::miria)
			TextureAsset(L"miriaResult").draw();
		else if (m_data->mamaName == MamaName::momoka)
			TextureAsset(L"momokaResult").draw();

		font(string).draw(20, textPosY, Alpha(150));
	}
	
	else if (!m_data->isGet)
	{
		TextureAsset(L"resultFailure").draw();
		TextureAsset(L"kimootaFailure").resize(360, 639).draw(Window::Width() - TextureAsset(L"kimootaFailure").width + 180, Window::Height() - TextureAsset(L"kimootaFailure").height + 300);
		TextureAsset(L"failure").drawAt(Window::Center());
	}

	if (countText >= 300)
	{
		font(L"Press Space to Title").drawAt(Window::Center() + Point(0, 200),Palette::Black);
	}
}