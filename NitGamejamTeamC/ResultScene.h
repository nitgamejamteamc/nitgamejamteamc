#pragma once
#include "MyAppDefine.h"

struct Result : MyApp
{
public:

	Result();
	~Result() = default;

	void init() override;
	void update() override;
	void draw() const override;

private:
	Font font;
	TextReader textreader{ L"textTest/result.txt" };
	String string;
	int textPosY;
	int countText;
};