#include "SystemMain.h"
#include "TitleScene.h"
#include "GameScene.h"
#include "Prolog.h"
#include "ResultScene.h"

SystemMain::SystemMain()
{
	//eNX`Μo^

	//UI
	TextureAsset::Register(L"Arrow0", L"textureTest/mae.png");
	TextureAsset::Register(L"Arrow1", L"textureTest/migi.png");
	TextureAsset::Register(L"Arrow2", L"textureTest/ushiro.png");
	TextureAsset::Register(L"Arrow3", L"textureTest/hidari.png");
	TextureAsset::Register(L"title", L"textureTest/title_k.png");
	TextureAsset::Register(L"gatya", L"textureTest/gatya.png");
	TextureAsset::Register(L"kimoota", L"textureTest/kimoota.png");
	TextureAsset::Register(L"momokaResult", L"textureTest/momoka_result.png");
	TextureAsset::Register(L"miyuResult", L"textureTest/miyu_result.png");
	TextureAsset::Register(L"miriaResult", L"textureTest/miria_result.png");
	TextureAsset::Register(L"mamaTitle", L"textureTest/mamaTitle.png");
	TextureAsset::Register(L"resultlogo",L"textureTest/resultlogo.png");
	TextureAsset::Register(L"failure", L"textureTest/failure.png");
	TextureAsset::Register(L"kimootaSabun", L"textureTest/kimootaSabun.png");
	TextureAsset::Register(L"miyuCurtain", L"textureTest/miyu.png");
	TextureAsset::Register(L"miriaCurtain", L"textureTest/miria.png");
	TextureAsset::Register(L"momokaCurtain", L"textureTest/momoka.png");
	TextureAsset::Register(L"curtain", L"textureTest/stop_c.png");
	TextureAsset::Register(L"mamaSelect", L"textureTest/mama_select.png");
	TextureAsset::Register(L"miyuSelect", L"textureTest/miyu_select.png");
	TextureAsset::Register(L"miriaSelect", L"textureTest/miria_select.png");
	TextureAsset::Register(L"momokaSelect", L"textureTest/momoka_select.png");
	TextureAsset::Register(L"rightArrow", L"textureTest/rightArrow.png");
	TextureAsset::Register(L"leftArrow", L"textureTest/leftArrow.png");
	TextureAsset::Register(L"kimootaFailure", L"textureTest/kimootaFailure.png");
	

	//wi
	TextureAsset::Register(L"All", L"textureTest/cross.png");
	TextureAsset::Register(L"Left", L"textureTest/curve_left.png");
	TextureAsset::Register(L"Right", L"textureTest/curve_right.png");
	TextureAsset::Register(L"Up0", L"textureTest/straight.png");
	TextureAsset::Register(L"Up1", L"textureTest/straight2.png");
	TextureAsset::Register(L"None", L"textureTest/stop_none.png");
	TextureAsset::Register(L"RightLeft", L"textureTest/Tcross1.png");
	TextureAsset::Register(L"UpLeft", L"textureTest/Tcross2.png");
	TextureAsset::Register(L"UpRight", L"textureTest/Tcross3.png");
	TextureAsset::Register(L"resultFailure", L"textureTest/selectMum.png");
	TextureAsset::Register(L"treasureBox", L"textureTest/takarabako.png");
	TextureAsset::Register(L"treasureBoxOpened", L"textureTest/takarabako_open.png");
	TextureAsset::Register(L"Guide", L"textureTest/gatya.png");
	TextureAsset::Register(L"Hinto", L"textureTest/hintL.png");
	TextureAsset::Register(L"Coin", L"textureTest/yukitikoinn.png");
	TextureAsset::Register(L"Open", L"textureTest/open.png");
	TextureAsset::Register(L"Turn", L"textureTest/turn.png");

	//TEhΜo^
	SoundAsset::Register(L"ogyaru", L"soundTest/¨¬αι.wav");
	SoundAsset::Register(L"OnTheWay", L"soundTest/doutyuu.wav");
	SoundAsset::Register(L"gameclear", L"soundTest/gameclear.wav");
	SoundAsset::Register(L"gameover", L"soundTest/gameover.wav");
	SoundAsset::Register(L"footstepSE", L"soundTest/se_asioto.mp3");
	SoundAsset::Register(L"cursorSE", L"soundTest/se_cursor.mp3");
	SoundAsset::Register(L"curtainSE", L"soundTest/se_curtain.mp3");
	SoundAsset::Register(L"gachaSE", L"soundTest/se_gacha coin.wav");
	SoundAsset::Register(L"adidasSE", L"soundTest/se_open adidas.wav");
	SoundAsset::Register(L"kapuseruSE", L"soundTest/se_open kapuseru.mp3");
	SoundAsset::Register(L"treasureBoxSE", L"soundTest/se_open takarabako.mp3");
	SoundAsset::Register(L"selectSE", L"soundTest/se_serect.mp3");

	//SceneΜo^
	m_SceneManager.add<Title>(L"Title");
	m_SceneManager.add<Prolog>(L"Prolog");
	m_SceneManager.add<GameMain>(L"Game");
	m_SceneManager.add<Result>(L"Result");

	Window::Resize(1024, 768);

	TextureAsset::PreloadAll();
	SoundAsset::PreloadAll();
}

void SystemMain::MainLoop()
{
	m_SceneManager.init(L"Title");

	while (System::Update())
	{
		m_SceneManager.updateAndDraw();
	}
}