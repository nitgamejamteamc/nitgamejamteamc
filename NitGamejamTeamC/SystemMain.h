#pragma once

#include "MyAppDefine.h"

class SystemMain
{
public:
	SystemMain();
	~SystemMain() = default;

	void MainLoop();

private:
	SceneManager<String, CommonData> m_SceneManager;
};