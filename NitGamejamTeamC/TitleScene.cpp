#include "TitleScene.h"

Title::Title()
{

}

void Title::init()
{
	SoundAsset(L"OnTheWay").setLoopBySec(true, 16.187, SoundAsset(L"OnTheWay").lengthSec());
	font = Font(40);
	fontTitle = Font(50, L"���C���I");
	alpha = 0;
	alphaManager = increase;
	fontColor = Color(204, 204, 204, alpha);
	SoundAsset(L"OnTheWay").play(2.0s);
}

void Title::update()
{
	alphaFunction();

	if (Input::KeySpace.clicked)
	{
		SoundAsset(L"selectSE").play();
		changeScene(L"Prolog");
	}
}

void Title::draw() const
{
	TextureAsset(L"title").draw();
	font(L"Press Space").drawAt(Window::Width()-200,Window::Height()-50, fontColor);
}

void Title::alphaFunction()
{
	if (alphaManager == increase)
		alpha += 3;
	else if (alphaManager == decrease)
		alpha -= 3;

	if ((alpha > 255) && (alphaManager == increase))
	{
		alpha = 255;
		alphaManager = decrease;
	}
	else if ((alpha < 0) && (alphaManager == decrease))
	{
		alpha = 0;
		alphaManager = increase;
	}
	fontColor.a = alpha;
}