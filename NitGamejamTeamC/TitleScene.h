#pragma once
#include "MyAppDefine.h"

struct Title : MyApp
{
public:

	Title();
	~Title() = default;

	void init() override;
	void update() override;
	void draw() const override;

private:
	Font font;
	bool alphaManager;
	void alphaFunction();
	Font fontTitle;
	int alpha;
	Color fontColor;
};