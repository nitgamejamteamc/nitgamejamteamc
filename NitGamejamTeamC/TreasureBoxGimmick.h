#pragma once
# include "FloorClass.h"

class TreasureBox :public FloorBase
{
public:
	TreasureBox();
	virtual bool gimmick(Player* player, bool& isEvent, String& assetName);
	virtual void textureDraw() const;

private:
	const int money; /*コンストラクタでランダムに定める？*/
	bool examined; /*falseで未調べ、trueで調べ済*/
};